# A one-line bash script
# to grep all the sentences
# from a folder of plain txt files
# that use the given query.

# To run it:
# $ sh etherdump.grep.sh tracks

echo "~~~"
for i in ./etherdump/*.txt; do cat "${i}" | grep $1 && echo "~~~"; done

