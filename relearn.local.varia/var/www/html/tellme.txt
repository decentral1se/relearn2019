#name
the relearn.local web directory
 
#who
The relearn server team ;) (Anne and Manetta)
 
#what
All the files in this folder are accessible through the browser, when navigating to relearn.local or relearn.lan.
 
#when
Most of the work happened in the weeks before the Relearn session in Rotterdam. We worked remotely, using tmux sessions, which was a lot of fun! But next to us, there is a lot of work, notes, material and other things in this folder that are made by the group that joined the weekend in Rotterdam. And probably this folder will grow into many other directions, while visiting the other points on the curve.
 
#why
It's not really a "work", but the /var/www/html does a great job ;)
 
#how

 
