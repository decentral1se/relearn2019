<pre>
                            .---.                                                             
              __.....__     |   |      __.....__                          _..._    ,.--.      
          .-''         '.   |   |  .-''         '.                      .'     '. //    \     
.-,.--.  /     .-''"'-.  `. |   | /     .-''"'-.  `.           .-,.--. .   .-.   .\\    |     
|  .-. |/     /________\   \|   |/     /________\   \    __    |  .-. ||  '   '  | `'-)/      
| |  | ||                  ||   ||                  | .:--.'.  | |  | ||  |   |  |   /'  _    
| |  | |\    .-------------'|   |\    .-------------'/ |   \ | | |  | ||  |   |  |     .' |   
| |  '-  \    '-.____...---.|   | \    '-.____...---.`" __ | | | |  '- |  |   |  |    .   | / 
| |       `.             .' |   |  `.             .'  .'.''| | | |     |  |   |  |  .'.'| |// 
| |         `''-...... -'   '---'    `''-...... -'   / /   | |_| |     |  |   |  |.'.'.-'  /  
|_|         .-'''-.                                  \ \._,\ '/|_|     |  |   |  |.'   \_.'   
           '   _    \                                 `--'  `"         '--'   '--'            
         /   /` '.   \        __  __   ___   .--.   _..._                                     
        .   |     \  '       |  |/  `.'   `. |__| .'     '.   .--./)                          
.-,.--. |   '      |  '      |   .-.  .-.   '.--..   .-.   . /.''\\                           
|  .-. |\    \     / /  __   |  |  |  |  |  ||  ||  '   '  || |  | |                          
| |  | | `.   ` ..' /.:--.'. |  |  |  |  |  ||  ||  |   |  | \`-' /                           
| |  | |    '-...-'`/ |   \ ||  |  |  |  |  ||  ||  |   |  | /("'`                            
| |  '-             `" __ | ||  |  |  |  |  ||  ||  |   |  | \ '---.                          
| |                  .'.''| ||__|  |__|  |__||__||  |   |  |  /'""'.\                         
| |                 / /   | |_                   |  |   |  | ||     ||                        
|_|                 \ \._,\ '/                   |  |   |  | \'. __//                         
                     `--'  `"                    '--'   '--'  `'---'                          
                __.....__           .----.     .----.   __.....__                             
            .-''         '.          \    \   /    /.-''         '.                           
           /     .-''"'-.  `. .-,.--. '   '. /'   //     .-''"'-.  `. .-,.--.                 
          /     /________\   \|  .-. ||    |'    //     /________\   \|  .-. |                
       _  |                  || |  | ||    ||    ||                  || |  | |                
     .' | \    .-------------'| |  | |'.   `'   .'\    .-------------'| |  | |                
    .   | /\    '-.____...---.| |  '-  \        /  \    '-.____...---.| |  '-                 
  .'.'| |// `.             .' | |       \      /    `.             .' | |                     
.'.'.-'  /    `''-...... -'   | |        '----'       `''-...... -'   | |                     
.'   \_.'                     |_|                                     |_|                     

</pre>

# README

## a roaming server? 

We started to think of the Relearn server as a roaming device, where *roaming* is considered as *the ability of telecommunication networks to continue a specific service in a foreign network*.

In the previous editions, Relearn's infrastructure has mostly been local, with its infrastructure taken away once the session is over. 
As this year's Relearn will be travelling between multiple sessions, the network will travel as well and becomes a transversal mediating tool over time and places. As such, this roaming server will be a documentation device.

This is how we started to work on a travelling roaming server, as one of the shared infrastructures of the different Relearn sessions on the curve. It will travel from session to session, probably going from hand to hand in backpacks in buses and trains, at the back of a bike and on a seat in the metro. 

The server will be used to create a temporary network that supports local exchange and captures traces of the different sessions. 

A **roaming server** that ...

(a) **mediates in the moment**: it intensifies links between the participants of one session, 
    through shared tools, logs and connections;

(b) **mediates between sessions**: ready to roam;

(c) **mediates throughout the curve**: 
    being curious to be bridging affect, files, discussions and resources, 
    it wants to be read, copied, edited and re-published ... 
    supporting inter-relations and cross-pollinations ... 
    but acknowledges that although it shares a zone with a group of people, that not every* relates to every*.

How can we take the aspect of *roaming* as a way to relearn our understanding of *networks*? 

* How can we make tools with/on the server to inspect the current local network or leak/ve information from the current session? (a)
* How to set up and run a (roaming) promiscuous server in multiple other (local) networks? (b)
* How can we trigger relations between transmissions of files and of knowledge in the different relearn sessions? (c) 


## tools 

Next to setting up the network configurations, we installed and created a few tools to support the different forms of mediation and documentation.

(a) (during a session):

* index.html with *leaking* logs
* index of collaborative text documents (etherpad+etherdump and/or gobby) 
* inventory of questions to the group of the current session

(b) (between sessions):

* changelog
* documentation

(c) (throughout the curve):

* message of the day/hour (ssh login + index.html)
* tool for making inter-relations (index/navigation/data-structure/tags/)? TBD 

