# Relearn's roaming server - Documentation

<div style="color:magenta;">The roaming-server is work in progress</div>

## roaming LAN (local network)

LAN (Local Area Network) access:

* relearn.local or 192.168.1.100
* ssh through port 22

`ssh username@relearn.local`


    ssh relearn@relearn.local

Or after you've made your own account:

    ssh USERNAME@relearn.local

## roaming WAN (connected to the internet)

### relearn@varia 

In the weeks before Relearn in Rotterdam, the server was connected to the vvvvvvaria network and accessible by visiting relearn.vvvvvvaria.org.

It used a SSL (let's encrypt) certificate for this domain.

WAN (Wide Area Network) access:

* relearn.vvvvvvaria.org
* ssh through port 10101

`ssh username@relearn.vvvvvvaria.org -p 10101`

All requests to the *relearn.vvvvvvaria.org* subdomain on port 80 (HTTP) & 443 (HTTPS) were forwarded to the roaming-relearn server on the ip-address 192.168.1.100. The following nginx .conf file was used on the Varia server: 

<pre>
server {
        listen 80;
        server_name relearn.vvvvvvaria.org;
        return 301 https://$server_name$request_uri;
        }

server {
        listen 443 ssl;
        server_name relearn.vvvvvvaria.org;
        ssl_certificate /etc/letsencrypt/live/relearn.vvvvvvaria.org/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/relearn.vvvvvvaria.org/privkey.pem;

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers HIGH:!aNULL:!MD5;

        access_log /var/log/nginx/relearn.vvvvvvaria.org.log;
        error_log /var/log/nginx/relearn.vvvvvvaria.org.log;

        location / {
                proxy_pass http://192.168.1.100/;
                proxy_redirect  off;
                proxy_read_timeout      60s;

                proxy_set_header        Host    relearn.vvvvvvaria.org;
                proxy_set_header        X-Real-Ip       $remote_addr;
                proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        }
}
</pre>

### Connect the server to the wider internet

The following ports were opened:

* 80 for WAN http
* 443 for WAN https
* 10101 for WAN ssh
* 6523 for WAN Gobby

Possibly also port 9001 will need to be open for the Etherpad.


## Tools [a] (during a session)

### index.html 

(at /var/www/html)

The index.html explores the current (consensual? pseudo?) **connections** to the server.
We're inspecting different type of connections in the form of **logs**.

#### cronjob logs

* $ who
* $ last

We started to use cron to generate logfiles for the index page, to leak some information from the server through the browser. 

To inspect the cronjobs, first login as the relearn user (password `relearn`):

	$ su -- relearn 

Then open crontab, software to edit the cronjobs of this user:

	$ crontab -e 

## Tools [b] (between sessions)

## Documentation

### README.md 

An introduction of some of the ideas around the roaming server.

### documentation.md

(This file) :)

We started to write the documentation file while trying out various collaborative editing tools (https://textb.org, https://pad.vvvvvvaria.org, Gobby).

Currently the file sits in the /var/www/html/documentation folder.
We're editing it through Gobby. A cronjob copies the file over from the `/var/www/html/gobby/documentation` folder to `/var/www/html/documentation`. 

More information about how to edit this file can be found in the documentation about the Gobby below.

### changelog.txt

### tmux

terminal multiplexer

While working on the server, Anne and Manetta used tmux to share a shell remotely. :)

## Tools [c] (throughout the curve)

### Etherpad + Etherdump

#### Etherpad

To install nodejs i followed this: <https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-debian-9>
And to install etherpad-lite i followed this: <https://networksofonesown.constantvzw.org/etherbox/manual.html#install-etherpad>

Mysql is used as the database.
The installation sits in the folder `/opt/etherpad/`.

#### Etherdump

The etherdump is currently re-generated every **5 minutes**.

The etherdump is installed at `/var/www/etherdump`.

It creates the "dump" in the `/var/www/html/etherdump/` folder.

**Etherdump** is a command-line utility that extends the multi writing and publishing functionalities of the Etherpad by exporting the pads in multiple formats. This Etherdump installation (currently) converts pads into html, diffhtml (not working .... :\), txt and json documents.

The Etherdump on the Relearn server run on the **opt-in __ PUBLISH __ version** of the software. This version does not list all the pads by default in the Etherdump index, but only includes a pad when it is tagged with the __PUBLISH__ tag. This means that sharing a pad with the rest of the Relearn group(s) is an opt-in choice.

During Relearn in Rotterdam, a zip feature is added to the etherdump. It creates a zip file of all the etherdump files, which is available for downloading at the bottom of the etherdump index page. The command sits in the `/var/www/html/etherdump/cron.sh` script, which is the same script from where the etherdump is generated. A cronjob is activated to execute this script every 5 minutes. To edit this cronjob you can:

Login under the relearn user

	$ su -- relearn 

And open the cronjob list

	$ crontab -e 

##### Links

* Etherdump https://gitlab.constantvzw.org/aa/etherdump
* Varia's __PUBLISH__ branch https://gitlab.constantvzw.org/decentral1se/etherdump/tree/publish-vs-nopublish, (the commit with notes: https://gitlab.constantvzw.org/decentral1se/etherdump/commit/f9bb4444e239c78977643c0548b0300cfb8911b2 )

### Gobby

<https://gobby.github.io/>

<https://github.com/gobby/gobby/wiki>

Gobby is a collaborative text editor, that you install on your computer.
If you share the same LAN (local area network), you can connect either directly to someone else's computer.
It is also possible to use Gobby with a central server, either through a LAN or the WAN (wider area network, aka the internet). 

#### Install Gobby

1. install gobby

`sudo apt install gobby`

2. run gobby

3. connect to other Gobbiers! 

If you are in a local network with other Gobby users, or a Gobby server, you can connect to one of the servers that appear on the left.

The local relearn Gobby server appears as `root via wlp58s0 on IPv4`. 
You can connect to it with the password `relearn`.

#### Gobby set on relearn.local

Gobby runs in the /var/www/gobby folder.
There is also a plugin activates that makes a *dump* of all the documents in /var/www/html/gobby.

Extra features:

**md > html**

If you write markdown documents (with the .md extention), the server converts it to a .html file.
A cronjob runs every minute, and executes the script /var/www/html/gobby/md2html.sh. 


#### Gobby server

The server software for Gobby is called *Infinoted*.

How to install a Gobby server?

1. install infinoted

`sudo apt install infonoted` (on Debian or Ubuntu)

2. create the file /etc/xdg/infinoted.conf and add the following settings:

[infinoted]
security-policy=require-tls
certificate-file=/etc/xdg/infinoted.cert
key-file=/etc/xdg/infinoted.key
password=YOURPASSWORDHERE
autosave-interval=5
root-directory=/var/data/gobby/data
sync-directory=/var/data/gobby/export
sync-interval=120

3. run infinoted with --create-certificate --create-key to generate the certificate and key ($ infinoted --create-certificate --create-key)
4. start infinoted on every restart of the server. Edit the file /etc/rc.local and add this:

`# starts up infinoted (gobby server)`
`# configuration file is in /etc/xdg/infinoted.conf`
`infinoted &`

5. open the port that Gobby uses (6523) on the router

6. restart the server

7. start Gobby on your computer. Connect to the server using "direct connection"

## Relearn's Roaming Hardware

* raspberry pi €38,95 https://www.kiwi-electronics.nl/raspberry-pi/board-and-kits/raspberry-pi-3-model-b-plus
* electricity cable €9,95 (EU and UK ready) ;) https://www.kiwi-electronics.nl/raspberry-pi/raspberry-pi-stroomvoorzieningen/rpi-psu-5-1v-2-5a--eu-uk
* sd card 64gb(?) €9,20 https://www.dataio.nl/kingston-64gb-microsd-canvas-select-met-sd-adapter/
* usb stick backup €12,50 https://www.dataio.nl/kingston-64gb-usb-data-traveler-100-g3/
* a case ? [what will be our lego-city alternative ;) not sure yet ;) but we have a bit of time to find an interesting one yes! a transversal home for this server, hmmm :) Peter has this idea to put it on a radio-controlled toy car :) hahah, yes or with a magnet on a flixbus ;)]
* openwrt wifi router

Total Constant budget: 

|€      | |
|-------|----|
|€53,85 |Kiwi|
|€21,70 |Data IO|
|€75,55 |total|

Total Varia budget:

|€      | |
|-------|----|
|€? |Wifi Router|



# CHAT LOG 

-*-*-*-

hello, let's write here
very plain ;)
and no colors! 
no linenumbers :(
but i was curious to try it. 
there is a markdown export support button --> the m - nice 
how to write from a situated perspective ? from varia's current location & future information will be added. a networked Changelog : that keeps the change of the server + the context of the server ---- yes nice nice!
yes nice, good to not uni-document!

i mean we're documenting from the perspective of having a server that can accomodate the beginning of relearn
(i also love the fact that we're having this conversation on so many platforms at the same time, it makes me so happy to wander with you in all these interfaces)
haha, yes, so many textual interfaces! so many modes of chatting 
would it be an idea to keep the chatting parts?
let's keep them if they don't lose readers -- yes, we can always move it around, or down -- yes

shall we use dates to indicate evolution of the server over time ? dates of the sessions you mean? or of today? dates of writing i suppose. like in a Changelog on a server where one will write what they install on a server at a specific time -- yes good idea :) we can then keep all the information together (if it's written down) and still be able to find what we need -- yes! 
would it be an idea to split changelog/documentation? in two documents ? indeed it might be a good idea
we can keep the information about the server physical travels : x brought the server from varia to hacktiris , yes nice! including convivial notes.
shall we decide to comment all the information which no longer works (.conf files for instance) or create new ones each time and keep the old ones in a specific folder ? ah good point, hmm, we already made changes here and there. I think it's oke to just change and modify? We will all have our own ways of doing anyway i think? So things might change all the time, and then there is a lot to back up? Also: what is the "orginal" conf beyond a set of defaults from the debian community? you're right. and i don't want to reify the process, just make it flowing. yes indeed ! 

sorry i lost myself in the relearn pad on varia, so many good things there :) was there something specifically?
hey i started writing this little piece of text last week about the server as mediator, curious to hear what you think about that.. i think it's a really nice one

---
Roaming? The ability of a mobile network operator to automatically receive or make calls in an other foreign network, to send and receive data.

roaming/rondzwerven/? is it roaming in dutch ? yes, it translates as "wandering around", what is it in French? errer, vagabonder, parcourir, traîner. we use "parcourir" to search for files as well. "errer & vagabonder" would mean "wandering around", and "traîner" is more like hanging out.

---
CHATS

    hmm what can this be ! postcards from the inventory ? (sending email is easier than receiving email from a server's point of view) or something using xmpp/activity pub ? -- ah are you thinking outside the local network? outside the server? that is perhaps more (b)? because i'm thinking between participants of different sessions but one idea to spread the inventory would use the message of the day possibility in a server that is when one logs with ssh, one element of the relearn inventory pops up. it's simple. and perhaps we can  have something similar with the local index.html like you did on the relearn.vvvvvvaria.org
-- message of the day is really nice! (message of the hour) (yes!) but i'm curious how many people will be ssh-ing... ah yes! nice, we can connect the ssh login message with the index file message. 
-- one thing i'm curious about is how participants themselves could also make relations, or publish to others. But again, i think this can be related to the editing tool we're going to use? yes. and perhaps something related to (c) will emerge from the varia session ! good point :)

    (nice yes, this could actually be a nice thing for the Friday evening, one of the tables can be hosted by the roaming server! :) relearn 2015 style -- yes ! i won't be there yet but it's something that can be on the Friday evening in paris as well, and on the wall -- yes indeed, we need to prepare a set of questions then)

    <--- do we need a copyleft license statement for all files on the server? -- good question! not all files on the server are produced by the participants so perhaps we can just say something like "add "donotreuse" for instance to a file on the server if you don't want people to change it -- Not sure if we need to pre-structure this? Maybe we can include it as a question, instead as a statement? -- indeed we don't need to. it makes me think about the discussion we had at NWA about photos to take out of the server if one wished : indeed photos are a very visible traces of presence, and files in a folder are a different thing. i'm not sure it makes sense to erase files (outside pictures) if one doesn't want it re-use. i agree with you it's better to keep these things as questions as we know that generally files are not reused in these contexts, and that sometimes we can't even make sense of why they are in the folders in the first place :) -- But it would be good to leave a message about this on .... the index page? I'm not sure what the "central" page/place will be to communicate things. Maybe that depends on the collaborative editing tool that we will use... [i'm going to move our chat below, is that oke?] yes it's ok, it's a good design decision! :)

    <--- is this what we want? shared ownership over the server? everyone has sudo access + the hardware is owned by Relearn -- yes


    (i'm not there yet, i would like to say how can we see a server  -- is this the handing-over part? or the relations between the content of the sessions? maybe this is (c) 
    (my sentence is not about the handing-over part, it's really about (c) : it started when i was about to type "use a server" and then it felt weird to say so i tried to find another verb : hence create relation but i'm not sure it's the one). 
    indeed, being open for possible relations, but also for non-related traces? -- yes, it's not only about traces. perhaps i mean something about bridging things : affect(!), files, discussions, resources 

-*-*-*-