cd /var/www/html/gobby/
#sudo chown -R relearn:www-data /var/www/html/gobby/*
#chmod -R g+w /var/www/html/gobby/*

for i in $(find . -name '*.md'); do
	echo "... $i --> $i.html"
	pandoc -f markdown -t html -s -c /styles.css $i -o $i.html
done
