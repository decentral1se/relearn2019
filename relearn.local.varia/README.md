Hello!

This folder contains a selection of the files from the re-roaming server while being at Varia on the 7th, 8th and 9th of June.
Included are files and folders that hold traces of the days that we were together.

`/var/www/`

`/home/` (hidden files are removed as they might contain sensitive information)

`/etc/` (only the files that were changed by the group)

* Pictures can be found at `/var/www/html/pictures/relearn@varia`
* The Etherdump has been running from `/var/www/html/etherdump` (files are in the folder "publish")

But there is a lot more to be found!



