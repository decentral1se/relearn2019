# thinking of a growing glossary tool [Jo] - - - following discussions on the conductable pad + publishing-formats-table + ...

                                # adds words to a glossary file:
                        if '@growinggloss' in msg['body']:

                                # reply from the bot
                                self.send_message(mto=self.room,
                                                                  mbody="new word (s) added! > language in motion < new poetics > scoring togetherness {}!".format(msg['mucnick']),
                                                                  mtype='groupchat')

                                # Add message to log
                                message = '{}'.format(msg['body'].replace('@growinggloss',''))
                                log = 'glossary.txt'
                                log_path = os.path.join(self.output, log)
                                f = open(log_path, 'a+')
                                f.write(message+'\n')
                                f.close()


                        # retrieves a random term from the growing glossary file:
                        if '@glossary' in msg['body']:

                                import random
                                file = open('glossary.txt', 'r')
                                filelist=[]
                                for line in file:
                                        print(line)
                                        filelist.append(line)
                                print(filelist)

                                self.send_message(mto=self.room,
                                                
                                                                mbody= random.choice(filelist),
                                                                mtype='groupchat') 


                        # everytime someone makes a question, the not replies with another question mark:
                        if '?' in msg['body']:

                                import random
                                wordslist = ['Do you mean automatic autonomy?', 'You might as well call it botonomy..', 'Sym-poiesis (by Donna Haraway)', 'What if we would consider naming it Intersectional Technologies (Femke Snelting)', 'How do you relate to Affective Infrastrucures?', 'Or co-writing scores for moving in a common ground?', 'Experiment with practices of notation / attention / intention.']
                                
                                # reply from the bot
                                self.send_message(mto=self.room,
                                                                
                                                                mbody= random.choice(wordslist),
                                                                mtype='groupchat') 
                                                          
                                                                 

                                # Add message to log
                                message = '<p class="message">{}</p>'.format(msg['body'].replace('@bot',''))
                                log = 'log.html'
                                log_path = os.path.join(self.output, log)
                                f = open(log_path, 'a+')
                                f.write(message+'\n')
                                f.close()



