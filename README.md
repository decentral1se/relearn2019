# Relearn 2019

This summer, Relearn is back in the form of a curve, transversing multiple times and spaces. 
Curl yourselves this summer from, to and between Rotterdam, Brussels and Paris ! Read more about the curve.

Relearn 2019 is a curve, transversing multiple times and spaces.

These would be independently organized sessions based on the urgencies and affordances of each of the participating spaces. 
However, there would still be a common thread throughout the series, perhaps in the form of a transversal “reroam” 
but also in the form of a roaming server that is passed on to each subsequent event. 

It is possible to attend all sessions but not required, nor the goal, it is really the intention to have 
self-standing events which do not depend people joining a succession of events but still share commonalities.

<http://relearn.be/2019>


